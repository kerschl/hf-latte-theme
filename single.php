<?php
/* Template for displaying posts */
get_header()
?>
<div class="single">
	<div>
		<?php
		while (have_posts()) : the_post();
			the_content();
		endwhile
		?>
	</div>
</div>
<?php
get_footer();
