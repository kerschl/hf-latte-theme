<?php /* The footer added to each view */ ?>
</main>
<footer id="footer">
	<nav id="footer-menu">
		<?php if (has_nav_menu('footer-menu')) {
			wp_nav_menu([
				'theme_location'  => 'footer-menu',
				'container'       => false,
				'menu_id'         => 'footer-menu-list',
				'menu_class'         => 'menu-list'
				// https://developer.wordpress.org/reference/functions/wp_nav_menu/
			]);
		} ?>
	</nav>
	<small><?php bloginfo('name'); ?> - All Rights Reserved &copy; <?php echo date('Y'); ?></small>
</footer>

<?php wp_footer(); ?>
</body>

</html>
