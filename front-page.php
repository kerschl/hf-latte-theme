<?php
/* Template for displaying the front page */
get_header();
// display site
echo '<div class="front-page"><div>';
if (have_posts()) : while (have_posts()) : the_post();
		the_content();
	endwhile;
endif;
echo '</div></div>';
// display latest 5 posts
$latest_blog_posts = new WP_Query(array(
	'posts_per_page' => 5,
	'post_type' => 'post'
));
if ($latest_blog_posts->have_posts()) :
	while ($latest_blog_posts->have_posts()) :
		$latest_blog_posts->the_post();
		echo '<div class="post"><div>';
		the_content();
		echo '</div></div>';
	endwhile;
	echo '<div class="post">
		<div>
			<div style="padding: 10px;">
				<p style="text-align: center; ;">
					<a style="color: #000; text-decoration: none" href="/archiv">&larr; ÄLTERE BEITRÄGE</a>
				</p>
			</div>
		</div>
	</div>';
endif;
get_footer();
