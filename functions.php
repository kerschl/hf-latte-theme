<?php
// menus
function register_my_menus()
{
	// register main menu
	register_nav_menus(array(
		'main-menu' => 'Main Menu',
	));
	// register footer menu
	register_nav_menus(array(
		'footer-menu' => 'Footer Menu',
	));
}
add_action('init', 'register_my_menus');

// Removes Gutenberg blocks stylesheet
function wpassist_remove_block_library_css()
{
	wp_dequeue_style('wp-block-library');
}
add_action('wp_enqueue_scripts', 'wpassist_remove_block_library_css');

// Removes wp-emoji js
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

// Force enable sitemap (/wp-sitemap.xml)
add_filter('wp_sitemaps_enabled', '__return_true');
// Sitemap disable users and taxonomies in sitemap
add_filter('wp_sitemaps_add_provider', 'kama_remove_sitemap_provider', 10, 2);
function kama_remove_sitemap_provider($provider, $name)
{
	$remove_providers = ['users', 'taxonomies'];
	// disabling users archives
	if (in_array($name, $remove_providers)) {
		return false;
	}
	return $provider;
}
