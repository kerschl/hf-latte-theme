<?php
/* Template for displaying pages */
get_header()
?>
<div class="page">
	<div>
		<?php
		while (have_posts()) : the_post();
			the_content();
		endwhile
		?>
	</div>
</div>
<?php
get_footer();
