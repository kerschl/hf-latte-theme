<?php
/* Template for displaying the posts archive */
get_header();
?>
<div class="index"></div>
<?
$latest_blog_posts = new WP_Query(array(
	'post_type' => 'post'
));
if ($latest_blog_posts->have_posts()) : while ($latest_blog_posts->have_posts()) : $latest_blog_posts->the_post();
?>
		<div class="post">
			<div>
				<?php the_content() ?>
			</div>
		</div>
<?php
	endwhile;
endif;
get_footer();
