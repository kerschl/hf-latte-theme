<?php /* The header added to each view */ ?>
<!DOCTYPE html>
<html lang="de">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title><?php echo is_front_page() ? get_bloginfo('name') : wp_title('') ?></title>
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/normalize.css">
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/build/styles.css">
	<?php wp_head() ?>
</head>

<body <?php body_class(); ?>>
	<header id="header">
		<nav id="main-menu">
			<div class="main-menu-hamburger">
				<a id="main-menu-toggle" href="#!"><span></span></a>
			</div>
			<?php
			if (has_nav_menu('main-menu')) {
				wp_nav_menu([
					'theme_location'  => 'main-menu',
					'container'       => false,
					'menu_id'         => 'main-menu-list',
					'menu_class'      => 'menu-list'
					// https://developer.wordpress.org/reference/functions/wp_nav_menu/
				]);
			} ?>
		</nav>
		<script>
			var nav = document.getElementById('main-menu');
			document.getElementById('main-menu-toggle').addEventListener('click', function() {
				if (nav.classList.contains("visible")) {
					nav.classList.remove("visible");
				} else {
					nav.classList.add("visible");
				}
			});
			var submenus = document.getElementsByClassName("menu-item-has-children");
			for (var i = 0; i < submenus.length; i++) {
				submenus[i].firstElementChild.addEventListener('click', function(e) {
					var el = e.srcElement.parentNode.getElementsByClassName("sub-menu")[0];
					if (el.classList.contains("visible")) {
						el.classList.remove("visible");
					} else {
						el.classList.add("visible");
					}
				});
			}
		</script>
		<h2><?= get_bloginfo('name') ?></h2>
		<hr>
		<h3><?= get_bloginfo('description') ?></h3>
	</header>
	<main id="main">
